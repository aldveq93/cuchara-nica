'use strict';

function mainCtrl() {
    var win = window;
    var media = win.matchMedia("(min-width: 1025px)");
    getBrowserHeight(media);
    media.addListener(getBrowserHeight);
}

function getBrowserHeight(media) {
    var doc = document,
        win = window;
    var heigthWindow = win.innerHeight;

    if (media.matches) {
        doc.getElementById('encabezado').style.height = heigthWindow + 'px';
    } else {
        doc.getElementById('encabezado').style.height = 'auto';
    }
}

mainCtrl();